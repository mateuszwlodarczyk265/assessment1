package com.wlodarczyk.mateusz.felfelassessment1.entities;

import com.wlodarczyk.mateusz.felfelassessment1.interfaces.BotListener;

public class Bot {
    private static final int sLEFT_TURNS_COLUMN = 0;
    private static final int sRIGHT_TURNS_COLUMN = 1;
    private static final int sN_TURNS_ROW = 0;
    private static final int sE_TURNS_ROW = 1;
    private static final int sW_TURNS_ROW = 2;
    private static final int sS_TURNS_ROW = 3;
    private static final BotPosition.Directions[][] sTURNS_MAP = {
            //L                       //R
            {BotPosition.Directions.W, BotPosition.Directions.E}, //N
            {BotPosition.Directions.N, BotPosition.Directions.S}, //E
            {BotPosition.Directions.S, BotPosition.Directions.N}, //W
            {BotPosition.Directions.E, BotPosition.Directions.W}  //S
    };
    private static final String sEMPTY_SPACE = " ";

    private String[] mInputLines;
    private int[][] mLawn;
    private enum Movements {R, L, F}
    private BotListener mBotListener;

    public BotPosition botPosition;
    public String botName;

    public Bot(int[][] lawn, String[] inputLines, String botName, BotListener listener) {
        mLawn = lawn;
        mInputLines = inputLines;
        mBotListener = listener;

        this.botName = botName;
        botPosition = getExtractedBotMovements();
    }

    public void move(){
        char[] movesChars = mInputLines[1].toCharArray();

        for (char move : movesChars) {
            String moveString = String.valueOf(move);
            if (Movements.valueOf(moveString).equals(Movements.F)) {
                if (botPosition.direction == BotPosition.Directions.N && botPosition.y < mLawn[1].length) {
                    botPosition.y++;
                } else if (botPosition.direction == BotPosition.Directions.E && botPosition.x < mLawn[0].length) {
                    botPosition.x++;
                } else if (botPosition.direction == BotPosition.Directions.W && botPosition.x > 0) {
                    botPosition.x--;
                } else if (botPosition.direction == BotPosition.Directions.S && botPosition.y > 0) {
                    botPosition.y--;
                }
            } else if (Movements.valueOf(String.valueOf(moveString)).equals(Movements.L) || Movements.valueOf(moveString).equals(Movements.R)) {
                botPosition.direction = sTURNS_MAP[getDirectionRowIndexForDirection(botPosition.direction)][getTurnsColumnIndexForLR(Movements.valueOf(moveString))];
            }
            mBotListener.onMoved(this);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private int getTurnsColumnIndexForLR(Movements movement){
        return movement.equals(Movements.L) ? sLEFT_TURNS_COLUMN : sRIGHT_TURNS_COLUMN;
    }

    private int getDirectionRowIndexForDirection(BotPosition.Directions direction){
        int row = 0;

        if(direction.equals(BotPosition.Directions.N)){
            row = sN_TURNS_ROW;
        } else if (direction.equals(BotPosition.Directions.E)){
            row = sE_TURNS_ROW;
        } else if (direction.equals(BotPosition.Directions.W)){
            row = sW_TURNS_ROW;
        } else if (direction.equals(BotPosition.Directions.S)){
            row = sS_TURNS_ROW;
        }

        return row;
    }

    private BotPosition getExtractedBotMovements(){
        botPosition = new BotPosition();
        botPosition.x = Integer.parseInt(mInputLines[0].split(sEMPTY_SPACE)[0]);
        botPosition.y = Integer.parseInt(mInputLines[0].split(sEMPTY_SPACE)[1]);
        botPosition.direction = BotPosition.Directions.valueOf(mInputLines[0].split(sEMPTY_SPACE)[2]);

        return botPosition;
    }
}
