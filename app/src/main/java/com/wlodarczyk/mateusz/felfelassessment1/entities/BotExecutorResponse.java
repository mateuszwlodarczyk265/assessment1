package com.wlodarczyk.mateusz.felfelassessment1.entities;

import java.util.ArrayList;

public class BotExecutorResponse {

    public String text;
    public ArrayList<Bot> bots;

    public BotExecutorResponse(String text, ArrayList<Bot> bots){
        this.text = text;
        this.bots = bots;
    }
}
