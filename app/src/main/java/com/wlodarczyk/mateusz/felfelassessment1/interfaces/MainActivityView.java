package com.wlodarczyk.mateusz.felfelassessment1.interfaces;

import android.content.Context;

public interface MainActivityView {
    void setOutputText(String text);
    void onExecutionFinished();
    void setCheckboxChecked();
    Context getContext();
}
