package com.wlodarczyk.mateusz.felfelassessment1.helpers;

import com.wlodarczyk.mateusz.felfelassessment1.entities.Bot;
import com.wlodarczyk.mateusz.felfelassessment1.entities.BotExecutorResponse;
import com.wlodarczyk.mateusz.felfelassessment1.entities.BotPosition;
import com.wlodarczyk.mateusz.felfelassessment1.interfaces.BotListener;

import java.util.ArrayList;

import io.reactivex.ObservableEmitter;

public class BotExecutor implements BotListener {

    private static final int sLAWN_SIZE_INDEX = 0;
    private static final int sBOT_COMMANDS_LINES_NUMBER = 2;
    private static final String sESCAPE_CHAR = "\n";

    private int[][] mLawn;
    private ObservableEmitter<BotExecutorResponse> mEmitter;
    private ArrayList<Bot> mBots;

    public void executeInputFile(String inputFile, ObservableEmitter<BotExecutorResponse> emitter){
        mEmitter = emitter;
        String[] inputLines = inputFile.split(sESCAPE_CHAR);
        mLawn = getLawn(inputLines);
        mBots = new ArrayList<>();

        for(int i = 0; i < (inputLines.length - 1) / sBOT_COMMANDS_LINES_NUMBER; i ++){
            mBots.add(new Bot(mLawn, new String[]{inputLines[1 + (i * sBOT_COMMANDS_LINES_NUMBER)], inputLines[2 + (i * sBOT_COMMANDS_LINES_NUMBER)]}, String.valueOf(i), this));
        }

        for (Bot bot: mBots) {
            bot.move();
        }
    }

    private int[][] getLawn(String[] inputLines){
        String[] sizeStrings = inputLines[sLAWN_SIZE_INDEX].split(" ");
        int[] sizeIntegers = {0, 0};
        for(int x = 0; x < sizeStrings.length; x ++){
            sizeIntegers[x] = Integer.parseInt(sizeStrings[x]) + 1;
        }

        return new int[sizeIntegers[0]][sizeIntegers[1]];
    }

    @Override
    public void onMoved(Bot bot) {
        String outputLawnString = "";
        for(int y = mLawn[0].length; y >= 0; y --)
        for(int x = 0; x < mLawn.length + 1; x ++) {
            if(x == 0) {
                outputLawnString = outputLawnString.concat(sESCAPE_CHAR);
            }

            if(x == bot.botPosition.x && y == bot.botPosition.y){
                String stringToConcat = "";
                if(bot.botPosition.direction.equals(BotPosition.Directions.N)){
                    stringToConcat = " ^ ";
                } else if (bot.botPosition.direction.equals(BotPosition.Directions.E)){
                    stringToConcat = " > ";
                } else if (bot.botPosition.direction.equals(BotPosition.Directions.W)){
                    stringToConcat = " < ";
                } else if (bot.botPosition.direction.equals(BotPosition.Directions.S)){
                    stringToConcat = " V ";
                }

                stringToConcat = stringToConcat.concat(x + "," + y);

                outputLawnString = outputLawnString.concat(stringToConcat);
            } else {
                outputLawnString = outputLawnString.concat("   ");
            }
        }

        for (Bot botFromList : mBots) {
            outputLawnString = outputLawnString.concat(sESCAPE_CHAR + "========================");
            outputLawnString = outputLawnString.concat(sESCAPE_CHAR + botFromList.botName + " is at position: " + botFromList.botPosition.x  + " " + botFromList.botPosition.y + " " + botFromList.botPosition.direction);
        }
        mEmitter.onNext(new BotExecutorResponse(outputLawnString, mBots));
    }
}
