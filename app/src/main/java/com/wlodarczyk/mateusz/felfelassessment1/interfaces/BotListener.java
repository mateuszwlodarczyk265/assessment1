package com.wlodarczyk.mateusz.felfelassessment1.interfaces;

import com.wlodarczyk.mateusz.felfelassessment1.entities.Bot;

public interface BotListener {
    void onMoved(Bot bot);
}
