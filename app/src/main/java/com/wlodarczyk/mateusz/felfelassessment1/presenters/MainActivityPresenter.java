package com.wlodarczyk.mateusz.felfelassessment1.presenters;

import android.content.Context;
import android.os.IBinder;
import android.view.inputmethod.InputMethodManager;

import com.wlodarczyk.mateusz.felfelassessment1.entities.BotExecutorResponse;
import com.wlodarczyk.mateusz.felfelassessment1.helpers.BotExecutor;
import com.wlodarczyk.mateusz.felfelassessment1.interfaces.MainActivityView;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class MainActivityPresenter {

    private BotExecutor mBotExecutor;
    private MainActivityView mView;
    private BotExecutorResponse mExecutorResponse;

    public MainActivityPresenter(MainActivityView view){
        mView = view;
    }

    public void onCreate(){
        mBotExecutor = new BotExecutor();
    }

    public void onExecuteClicked(final String text, IBinder windowToken, String expectedOutput){
        hideKeyboard(windowToken);

        final String[][] expectedOutputsArray = getExpectedOutputsArray(expectedOutput);


        Observable<BotExecutorResponse> executeInputFile = Observable.create(new ObservableOnSubscribe<BotExecutorResponse>() {
            @Override
            public void subscribe(@NonNull ObservableEmitter<BotExecutorResponse> emitter) throws Exception {
                mBotExecutor.executeInputFile(text, emitter);
                emitter.onComplete();
            }
        });

        executeInputFile.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<BotExecutorResponse>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull BotExecutorResponse response) {
                        mView.setOutputText(response.text);
                        mExecutorResponse = response;
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                        mView.onExecutionFinished();
                        if(isOutputCorrect(expectedOutputsArray, getStringArrayFromResponse(mExecutorResponse))){
                            mView.setCheckboxChecked();
                        }
                    }
                });
    }

    private void hideKeyboard(IBinder windowToken) {
        InputMethodManager imm = (InputMethodManager) mView.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(windowToken, InputMethodManager.HIDE_NOT_ALWAYS);
    }

    @android.support.annotation.NonNull
    private String[][] getExpectedOutputsArray(String expectedOutput) {
        String[] expectedOutputLines = expectedOutput.split("\n");

        final String[][] expectedOutputsArray = new String[expectedOutputLines.length][3];

        for(int i = 0; i < expectedOutputLines.length; i ++) {
            expectedOutputsArray[i] = expectedOutputLines[i].split(" ");
        }
        return expectedOutputsArray;
    }

    private String[][] getStringArrayFromResponse(BotExecutorResponse response){
        String[][] stringArray = new String[response.bots.size()][3];
        for (int i = 0; i < response.bots.size(); i ++) {
            stringArray[i][0] = String.valueOf(response.bots.get(i).botPosition.x);
            stringArray[i][1] = String.valueOf(response.bots.get(i).botPosition.y);
            stringArray[i][2] = String.valueOf(response.bots.get(i).botPosition.direction);
        }

        return stringArray;
    }
    private boolean isOutputCorrect(String[][] expectedOutput, String[][] actualOutput){
        boolean isOutputCorrect = true;
        if(expectedOutput.length != actualOutput.length){
            return false;
        }

        for(int i = 0; i < expectedOutput.length; i ++){
            if(expectedOutput[i].length != actualOutput[i].length){
                isOutputCorrect = false;
                break;
            }

            for(int j = 0; j < expectedOutput[i].length; j ++){
                if(!expectedOutput[i][j].equals(actualOutput[i][j])){
                    isOutputCorrect = false;
                    break;
                }
            }
        }

        return isOutputCorrect;
    }
}
