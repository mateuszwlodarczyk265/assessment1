package com.wlodarczyk.mateusz.felfelassessment1.entities;

public class BotPosition {
    public int x, y;
    public Directions direction;
    public enum Directions {N, E, W, S}
}
