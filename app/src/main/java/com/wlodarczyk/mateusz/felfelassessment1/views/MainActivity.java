package com.wlodarczyk.mateusz.felfelassessment1.views;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.wlodarczyk.mateusz.felfelassessment1.R;
import com.wlodarczyk.mateusz.felfelassessment1.interfaces.MainActivityView;
import com.wlodarczyk.mateusz.felfelassessment1.presenters.MainActivityPresenter;

public class MainActivity extends AppCompatActivity implements MainActivityView {

    private MainActivityPresenter mPresenter = new MainActivityPresenter(this);

    private TextView mOutputTextView;
    private EditText mInputEditText, mExpectedOutputEditText;
    private boolean mIsExecuting;
    private CheckBox mCheckBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mCheckBox = (CheckBox) findViewById(R.id.checkBox);
        mOutputTextView = (TextView) findViewById(R.id.outputTextView);
        mExpectedOutputEditText = (EditText) findViewById(R.id.expectedOutputEditText);
        mInputEditText = (EditText) findViewById(R.id.inputFileEditText);
        mInputEditText.setText("5 5\n" +
                "1 2 N\n" +
                "LFLFLFLFF\n" +
                "3 3 E\n" +
                "FFRFFRFRRF\n" +
                "3 5 W\n" +
                "FFLFRFFL");

        mExpectedOutputEditText.setText("1 3 N\n" +
                "5 1 E\n" +
                "0 4 S");
        Button executeButton = (Button) findViewById(R.id.executeButton);
        mPresenter.onCreate();
        executeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!mIsExecuting) {
                    mIsExecuting = true;
                    mCheckBox.setChecked(false);
                    mPresenter.onExecuteClicked(mInputEditText.getText().toString(), findViewById(android.R.id.content).getWindowToken(), mExpectedOutputEditText.getText().toString());
                }
            }
        });
    }

    @Override
    public void setOutputText(String text) {
        mOutputTextView.setText(text);
    }

    @Override
    public void onExecutionFinished() {
        mIsExecuting = false;
    }

    @Override
    public void setCheckboxChecked() {
        mCheckBox.setChecked(true);
    }

    @Override
    public Context getContext() {
        return this;
    }
}
